package cz.kolarvit.baconator.service;

import cz.kolarvit.baconator.exception.BaconException;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.logging.Level;

@Service
@Log
public class ExternalBaconService {

    public String getBaconData() {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://baconipsum.com/api/?type=all-meat&paras=1"))
                    .GET()
                    .build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            int statusCode = response.statusCode();
            if (Response.Status.Family.familyOf(statusCode) != Response.Status.Family.SUCCESSFUL) {
                throw new BaconException("Response is not successful. Code: " + statusCode);
            }
            return response.body();
        } catch (InterruptedException | IOException e) {
            handleException(e);
        }
        return null;
    }

    private void handleException(Throwable e) {
        System.out.println("An error occurred: " + e.getMessage());
        log.log(Level.SEVERE, "An error occurred: " + e.getMessage(), e);
    }
}
