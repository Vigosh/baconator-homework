package cz.kolarvit.baconator.service;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Service for counting words for histogram.
 */
@Service
public class BaconWordCounter {

    private ConcurrentMap<String, Integer> wordMap = new ConcurrentHashMap<>();

    public void calculateAndStoreWordCount(String baconData) {
        for (String word : getWords(baconData)) {
            increaseCount(word);
        }
    }

    public Map<String, Integer> getWordCountMap() {
        return sortByValue(wordMap, true);
    }

    private void increaseCount(String word) {
        Integer count = wordMap.get(word);
        if (count == null) {
            count = 1;
        } else {
            count++;
        }
        wordMap.put(word, count);
    }

    private List<String> getWords(String input) {
        List<String> words = new ArrayList<>();
        if (input != null && !input.isEmpty()) {
            Pattern pattern = Pattern.compile("(\\b[^\\s]+\\b)");
            Matcher matcher = pattern.matcher(input);
            while (matcher.find()) {
                words.add(matcher.group().toLowerCase());
            }
        }
        return words;
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map, boolean desc) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());
        if (desc) {
            Collections.reverse(list);
        }
        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
}
