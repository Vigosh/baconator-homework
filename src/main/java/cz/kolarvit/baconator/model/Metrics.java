package cz.kolarvit.baconator.model;

import lombok.Data;

import java.util.Map;

@Data
public class Metrics {

    private Map<String, Integer> requests;
    private Map<String, Integer> words;
}
