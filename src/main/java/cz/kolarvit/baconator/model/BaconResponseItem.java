package cz.kolarvit.baconator.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.Duration;
import java.time.Instant;

@Data
@JsonPropertyOrder({"start", "end", "duration", "data"})
public class BaconResponseItem {

    private Long start;
    private Long end;
    private String data;

    public BaconResponseItem() {
        this.start = Instant.now().toEpochMilli();
    }

    public void setData(String data) {
        this.data = data;
        this.end = Instant.now().toEpochMilli();
    }

    @JsonGetter
    public String getDuration() {
        Duration duration = Duration.between(Instant.ofEpochMilli(start), Instant.ofEpochMilli(end));
        return String.format("%dh %02dm %02ds %02dms",
                duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart(), duration.toMillisPart());
    }
}
