package cz.kolarvit.baconator.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@JsonPropertyOrder({"runId", "start", "end", "duration", "items"})
public class BaconResponse {

    private String runId;
    private Long start;
    private Long end;
    private List<BaconResponseItem> items = new ArrayList<>();

    public BaconResponse() {
        this.runId = UUID.randomUUID().toString();
        this.start = Instant.now().toEpochMilli();
    }

    public void addItem(BaconResponseItem item) {
        this.items.add(item);
        this.end = Instant.now().toEpochMilli();
    }

    @JsonGetter
    public String getDuration() {
        Duration duration = Duration.between(Instant.ofEpochMilli(start), Instant.ofEpochMilli(end));
        return String.format("%dh %02dm %02ds %02dms",
                duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart(), duration.toMillisPart());
    }
}
