package cz.kolarvit.baconator.endpoint;

import cz.kolarvit.baconator.model.Metrics;
import cz.kolarvit.baconator.service.BaconWordCounter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Service for providing statistics and metrics data.
 */
@Service
@Path("")
public class MetricsService {

    @Autowired
    private BaconWordCounter baconWordCounter;

    @GET
    @Path("/metrics")
    @Produces(MediaType.APPLICATION_JSON)
    public Metrics getMetrics() {
        Metrics metrics = new Metrics();
        metrics.setWords(baconWordCounter.getWordCountMap());
        return metrics;
    }
}
