package cz.kolarvit.baconator.endpoint;

import com.fasterxml.jackson.databind.SerializationFeature;
import cz.kolarvit.baconator.exception.BaconException;
import cz.kolarvit.baconator.model.BaconResponse;
import cz.kolarvit.baconator.model.BaconResponseItem;
import cz.kolarvit.baconator.service.BaconWordCounter;
import cz.kolarvit.baconator.service.ExternalBaconService;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.java.Log;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.annotation.JacksonFeatures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.logging.Level;

/**
 * Main service for getting bacons.
 */
@Service
@Path("/give-me-bacon")
@Log
public class BaconService {

    @Autowired
    BaconWordCounter baconWordCounter;

    @Autowired
    private ExternalBaconService externalBaconService;

    /**
     * Get random bacons.
     *
     * @param howMuch number of desired items
     * @return json data
     */
    @GET
    @Path("{howmuch}")
    @Produces(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = {SerializationFeature.INDENT_OUTPUT})
    public BaconResponse getBacon(@PathParam("howmuch") Integer howMuch) {
        try {
            validateInputParam(howMuch);
            BaconResponse baconResponse = new BaconResponse();
            Flowable<Integer> range = Flowable.range(0, howMuch);
            range.parallel()
                    .runOn(Schedulers.io())
                    .map(v -> addBaconItemToResponse(baconResponse))
                    .sequential()
                    .blockingSubscribe(this::handleReceivedData);

            return baconResponse;
        } catch (Exception e) {
            handleException(e);
            return null;
        }
    }

    private void handleReceivedData(BaconResponseItem baconResponseItem) {
        System.out.println("Received " + baconResponseItem);
        baconWordCounter.calculateAndStoreWordCount(baconResponseItem.getData());
    }

    private BaconResponseItem addBaconItemToResponse(BaconResponse baconResponse) {
        BaconResponseItem item = new BaconResponseItem();
        item.setData(externalBaconService.getBaconData());
        baconResponse.addItem(item);
        return item;
    }

    private void validateInputParam(Integer count) {
        if (count == null) {
            throw new BaconException("Bacon count cannot be null");
        } else if (count <= 0) {
            throw new BaconException("Bacon count must be upon zero");
        }
    }

    private void handleException(Throwable e) {
        System.out.println("An error occurred: " + e.getMessage());
        log.log(Level.SEVERE, "An error occurred: " + e.getMessage(), e);
    }
}
