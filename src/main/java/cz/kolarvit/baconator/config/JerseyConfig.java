package cz.kolarvit.baconator.config;

import cz.kolarvit.baconator.endpoint.BaconService;
import cz.kolarvit.baconator.endpoint.MetricsService;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(BaconService.class);
        register(MetricsService.class);
        property(ServletProperties.FILTER_FORWARD_ON_404, true);
    }
}
