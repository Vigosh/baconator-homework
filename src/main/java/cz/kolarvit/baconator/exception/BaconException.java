package cz.kolarvit.baconator.exception;

public class BaconException extends RuntimeException {

    public BaconException() {
    }

    public BaconException(String message) {
        super(message);
    }

    public BaconException(Throwable cause) {
        super(cause);
    }

    public BaconException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaconException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
